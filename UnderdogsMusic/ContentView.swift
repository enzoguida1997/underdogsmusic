//
//  ContentView.swift
//  UnderdogsMusic
//
//  Created by Vincenzo Guida on 21/02/2020.
//  Copyright © 2020 Vincenzo Guida. All rights reserved.
//

import SwiftUI
import AudioKit

struct ContentView: View {
    
    @State var selected = 0
    
    var body: some View {
        
        VStack{
            Button(action: {
                // What to perform
            }) {
                Text("Sopra")
            }
            
            Button(action: {
                // What to perform
            }) {
                Text("Sotto")
            }
            
            Button(action: {
                // What to perform
            }) {
                Text("Destra")
            }
            
            Button(action: {
                // What to perform
            }) {
                Text("Sinistra")
            }
            
            Button(action: {
                // What to perform
            }) {
                Text("Rec")
            }
            
            Button(action: {
                // What to perform
            }) {
                Text("Play")
            }
            
        }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

