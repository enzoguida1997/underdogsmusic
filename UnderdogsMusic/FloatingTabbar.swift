////
////  FloatingTabbar.swift
////  UnderdogsMusic
////
////  Created by Vincenzo Guida on 21/02/2020.
////  Copyright © 2020 Vincenzo Guida. All rights reserved.
////
//
//import SwiftUI
//
//struct FloatingTabbar : View {
//    
//    @Binding var selected : Int
//    @State var expand1 = false
//    
//    @State var selectedImage = "Group 3625"
//    
//    var body : some View{
//        
//        HStack(){
//            
//            Spacer()
//            
//            HStack{
//                
//                if !self.expand{
//                    
//                    Button(action: {
//                        
//                        self.expand.toggle()
//                        
//                    }) {
//                        VStack{
//                            Image(selectedImage).foregroundColor(.black)
//                            Spacer()
//                            .frame(height: 10)
//                            Image(selectedImage).foregroundColor(.black)
//                        }
//                    }
//                }
//                else{
//                    
//                    Button(action: {
//                        
//                        self.selected = 0
//                        
//                    }) {
//                        
//                        Image("Group 3624").foregroundColor(self.selected == 0 ? .black : .gray).padding(.horizontal)
//                    }
//                    
//                    Spacer()
//                        .frame(height: 100)
//                    
//                    Button(action: {
//                        
//                        
//                        self.expand.toggle()
//                        //                        self.selectedImage = "Group3624"
//                        
//                    }) {
//                        
//                        Image("Group 3623").foregroundColor(self.selected == 1 ? .black : .gray).padding(.horizontal)
//                    }
//                    
//                    Spacer()
//                        .frame(height: 100)
//                    
//                    Button(action: {
//                        self.selected = 1
//                        //                        self.selectedImage = "Group 3625"
//                        
//                    }) {
//                        //                        FloatingTabbar(selected: self.$selected)
//                        Image("Group 3625").foregroundColor(self.selected == 2 ? .black : .gray).padding(.horizontal)
//                    }
//                }
//                
//                
//            }.padding(.vertical,self.expand ? 20: 8)
//                .padding(.horizontal,self.expand ? 20 : 8)
//                .background(Color.white)
//                .clipShape(Capsule())
//                .padding(30)
//                .onLongPressGesture {
//                    
//                    self.expand.toggle()
//            }
//                
//                //            .animation(.interactiveSpring(response: 0.6, dampingFraction: 0.6, blendDuration: 0.6))
//                .animation(.easeIn(duration: 0.2))
//        }
//        
//        
//    }
//}
//
